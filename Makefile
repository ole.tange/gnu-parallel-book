GNU_parallel_2019_with_mirrored_end.pdf: mirror.pdf GNU_Parallel_2019.pdf
	pdftk GNU_Parallel_2019.pdf /tmp/mirror6.pdf cat output GNU_parallel_2019_with_mirrored_end.pdf

mirror.pdf: GNU_Parallel_2019_mirror_end.pdf
	mirrorpdf GNU_Parallel_2019_mirror_end.pdf
	mv GNU_Parallel_2019_mirror_end_mirror.pdf mirror.pdf
	# Generate /tmp/mirror{1..20}.pdf
	seq 20 | parallel pdftk mirror.pdf cat {}-1 output /tmp/mirror{}.pdf

clean:
	rm GNU_parallel_2019_with_mirrored_end.pdf mirror.pdf

#pdftk GNU_Parallel_2019.pdf dump_data | perl -ne '/NumberOfPages:\s*(.*)/ and print "$1\n"'

# Does not insert auto generated empty pages
#	libreoffice --headless --convert-to pdf GNU_parallel_2019.fodt

